bookmarks_2
8.06.2022.html
CORE
https://andreasbm.github.io/web-skills/
https://my-js.org/docs/guide/intro-guide
https://doka.guide/
https://metanit.com/
 
CSS
https://htmlbase.ru/
https://github.com/yoksel
https://yoksel.github.io/flex-cheatsheet/
https://yoksel.github.io/grid-cheatsheet/
https://www.w3schools.com/css/default.asp
https://www.w3schools.com/howto/default.asp
 
Other
https://docs.arealidea.com/development/programming/nestjs.html
 
node
https://github.com/utimur/node-js-fundamental
https://github.com/YauhenKavalchuk/node-js
 
nest normal
ulbi
https://github.com/utimur/profi-backend-node-js
https://github.com/utimur/music-platform-course
best
https://github.com/lujakob/nestjs-realworld-example-app/tree/master/src
https://github.com/AlariCode/top-api-demo
sample
https://github.com/nestjs/nest/tree/master/sample
https://github.com/tkssharma/nestjs-advance-course
sequelize
https://github.com/onwuvic/nest-blog-api
https://github.com/kentloog/nestjs-sequelize-typescript
 
nest hard
https://github.com/vitaly-sazonov/nodejs2021Q4-service
https://github.com/vitaly-sazonov/kanban-rest
https://github.com/Jon-Peppinck/linkedin-clone
//////
https://github.com/ThomasOliver545/Blog-with-NestJS-and-Angular
https://github.com/mogilevtsevdmitry/angular-nestjs-postgresql-typeorm-graphql-docker
https://github.com/js-code-ua/nestjs-demo/tree/auth-endpoints
///////
https://nestjs.ru.com/guide/first-steps.html
https://github.com/nestjs/awesome-nestjs
https://www.darraghoriordan.com/2021/11/03/nest-cheatsheet-interceptor-middleware-guard/
https://habr.com/ru/post/439434/
https://wanago.io/2020/05/11/nestjs-api-controllers-routing-module/
https://www.freecodecamp.org/news/build-web-apis-with-nestjs-beginners-guide/
 
https://victoronwuzor.medium.com/how-to-setup-sequelize-migration-in-a-nestjs-project-b4aec1f88612
https://github.com/RobinBuschmann/sequelize-typescript#many-to-many
https://habr.com/ru/post/488054/
https://habr.com/ru/post/566036/
 
https://stackoverflow.com/questions/43729254/sequelize-limit-and-offset-incorrect-placement-in-query
 
https://www.tomray.dev/nestjs-docker-production
 
microservices
https://codesandbox.io/s/zmy3c?file=/src/user/user.service.ts
https://github.com/benjsicam/nestjs-rest-microservices