import API from "@/utils/API";
import { ref, isRef, unref, watchEffect } from "vue";

export function useAxios(url) {
  const data = ref(null);
  const error = ref(null);

  async function doFetch() {
    // reset state before fetching..
    data.value = null;
    error.value = null;
    // unref() unwraps potential refs
    try {
      const { data: getData } = await API.get(unref(url));
      data.value = getData;
    } catch (e) {
      error.value = e;
    }
  }
  if (isRef(url)) {
    // setup reactive re-fetch if input URL is a ref
    watchEffect(doFetch);
  } else {
    // otherwise, just fetch once
    // and avoid the overhead of a watcher
    doFetch();
  }

  return { data, error };
}
