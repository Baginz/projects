const PUBLIC_ENDPOINTS = [
  "/auth/login",
  "/auth/logout",
  "/auth/statusToken",
  "/auth/refreshToken",
];
export default PUBLIC_ENDPOINTS;
