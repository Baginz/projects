import { createRouter, createWebHistory } from "vue-router";
import HomeView from "@/views/HomeView.vue";
import HelloView from "@/views/HelloView.vue";
import ExamplesView from "@/views/ExamplesView.vue";
import PiniaView from "@/views/PiniaView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Home",
      component: HomeView,
    },
    {
      path: "/hello",
      name: "Hello",
      component: HelloView,
    },
    {
      path: "/examples",
      name: "Examples",
      component: ExamplesView,
    },
    {
      path: "/pinia",
      name: "Pinia",
      component: PiniaView,
    },
    {
      path: "/about",
      name: "About",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
  ],
});

router.beforeEach((to, from, next) => {
  next();
});

router.afterEach((to, from, next) => {
  console.log(router.currentRoute.value.name);
  document.title = router.currentRoute.value.name || "hello";
});

export default router;
