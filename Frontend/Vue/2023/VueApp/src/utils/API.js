import axios from "axios";
// import PUBLIC_ENDPOINTS from "../constants";

/**
 * Api класс для отправки запросов на сервер
 */
class Api {
  /**
   * Создание нового экземпляра Api
   * @param {string} SERVER_BASE_URL - адрес сервера для отправки запросов
   */
  constructor(SERVER_BASE_URL) {
    this.instance = axios.create({
      baseURL: SERVER_BASE_URL,
      responseType: "json",
    });

    /**
     * Отправка Post запроса
     * @param {string} url
     * @param {any} data
     * @param {AxiosRequestConfig<any>||{}} config
     * @return {Promise<AxiosResponse<any>>}
     */
    this.post = async (url, data, config = {}) => {
      const cfg = { ...config };
      if (!cfg.headers) {
        cfg.headers = {};
      }
      // cfg.headers.authorization = await this.token(url);
      return this.instance.post(url, data, cfg);
    };

    /**
     * Отправка Patch запроса
     * @param {string} url
     * @param {any} data
     * @param {AxiosRequestConfig<any>||{}} config
     * @return {Promise<AxiosResponse<any>>}
     */
    this.patch = async (url, data, config = {}) => {
      const cfg = { ...config };
      if (!cfg.headers) {
        cfg.headers = {};
      }
      // cfg.headers.authorization = await this.token(url);
      return this.instance.patch(url, data, cfg);
    };

    /**
     * Отправка Put запроса
     * @param {string} url
     * @param {any} data
     * @param {AxiosRequestConfig<any>||{}} config
     * @return {Promise<AxiosResponse<any>>}
     */
    this.put = async (url, data, config = {}) => {
      const cfg = { ...config };
      if (!cfg.headers) {
        cfg.headers = {};
      }
      // cfg.headers.authorization = await this.token(url);
      return this.instance.put(url, data, cfg);
    };

    /**
     * Отправка Get запроса
     * @param {string} url
     * @param {AxiosRequestConfig<any>||{}} config
     * @return {Promise<AxiosResponse<any>>}
     */
    this.get = async (url, config = {}) => {
      const cfg = { ...config };
      if (!cfg.headers) {
        cfg.headers = {};
      }
      // cfg.headers.authorization = await this.token(url);
      return this.instance.get(url, cfg);
    };

    /**
     * Отправка Delete запроса
     * @param {string} url
     * @param {AxiosRequestConfig<any>||{}} config
     * @return {Promise<AxiosResponse<any>>}
     */
    this.delete = async (url, config = {}) => {
      const cfg = { ...config };
      if (!cfg.headers) {
        cfg.headers = {};
      }
      // cfg.headers.authorization = await this.token(url);
      return this.instance.delete(url, cfg);
    };
  }
}
export default new Api(import.meta.env.VITE_SERVER_BASE_URL);
