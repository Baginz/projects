// Boolean Type
let isCompleted: boolean = false;

// Number Type
const decimal: number = 6;
const integer: number = 7.10;
const hex: number = 0xf00d;
const binary: number = 0b1010;
const octal: number = 0o744;

// String Type for simple string
const name: string = 'Yauhen';
// String Type for template string
const sentence: string = `Hello, my name is ${ name }!`;

// TypeScript types:
const u: undefined = undefined;
const n: null = null;

// Void Type
// For function result:
const greetUser = (): void => {
    alert("Hello, nice to see you!");
};

// Array Type
let list: number[] = [1, 2, 3];
let list: Array<number> = [1, 2, 3];	// Generic type

// Tuple Type
// Multiple lines
let y: [string, number] = ["goodbuy", 42];
// Any Type
// Any type for array
let y: [any, any] = ["goodbuy", 42];
let z: Array<any> = [10, "hello"];

// Enum Type
enum Directions {
    Up,
    Down,
    Left,
    Right
}
Directions.Up;      // 0
Directions.Down;    // 1
Directions.Left;    // 2
Directions.Right;   // 3
// Custom index for enum elements
enum Directions {
    Up = 2,
    Down = 4,
    Left = 6,
    Right
}
Directions.Up;      // 2
Directions.Down;    // 4
Directions.Left;    // 6
Directions.Right;   // 7

// Never Type
// Function return Error
const msg = "hello";
const error = (msg: string): never => {
    throw new Error(msg);
};
// Function infinite loop
const infiniteLoop = (): never => {
    while (true) {
    }
};

// Object Type
const create = (o: object | null): void => { };

// Multiple types for one value
let id: number | string;

// Type
type Name = string;	// Custom type creation

let id: Name;	// Apply custom type

// Arguments type
const createPassword = (name: string, age: number) => `${name}${age}`;

// Multiple argument types
const createPassword = (name: string, age: number | string) => `${name}${age}`;

// Default Arguments
const createPassword = (name: string = 'Max', age: number | string = 20) => `${name}${age}`;

// Function with optional argument 'age'
const createPassword = (name: string, age?: number) => `${name}${age}`;

// REST type
const createSkills = (name: string, ...skills: Array<string>) => `${name}, my skils are ${skills.join()}`;

// Returned type is string
const createPassword = (name: string, age: number | string): string => `${name}${age}`;

// Returned type is number
const sum = (first: number, second: number): number => first + second;

// Returned type is object
const createEmptyObject = (): object => ({});

// Define object type
let user: { name: string, age: number } = {
    name: 'Yauhen',
    age: 30,
};

// Updating type with optional properties
type Person = {
    name: string,
    age: number,
    nickName?: string,
    getPass?: () => string,
};
// 2 object with almost the same structure
let user: Person = {
    name: 'Yauhen',
    age: 30,
    nickName: 'webDev',			// <--- property
};

let admin: Person = {
    name: 'Max',
    age: 20,
    getPass(): string {			// <--- new method
        return `${this.name}${this.age}`;
    },
};